set terminal qt # Alternatives: qt (some OSX), wxt (gives top bar menu).
set grid
set key right top title ''

# Set linestyles (line-points, line, points):
set style line 1 \
    linecolor rgb '#098c00' \
    linetype 1 linewidth 1 \
    pointtype 1 pointsize 0.5

set style line 2 \
    linecolor rgb '#405cc2' \
    linetype 1 linewidth 1

set style line 3 \
    linecolor rgb '#f0133c' \
    pointtype 1 pointsize 1.5

set xlabel "Distance to Server (m)"
set ylabel "RSSI (dBm)"
set autoscale
# set initial value for n, the path loss constant for my living room
n = 2

# power is a function of the distance s
p(x) = -10 * n * log10(x) - 57.138798827827
h(x) = -10 * 1.1654146058219 * log10(x) - 57.138798827827

# Fit curve of path loss model to measurements via the path loss constant for my living room
fit p(x) 'combined_log_scaled_rssi.dat' using ($1):($2) via n

set samples 400     # More precise function plotting.

set title "Regression vs Antialising with Log Scaled RSSI of 5 Peripherals"
plot 'combined_log_scaled_rssi.dat' using ($1):($2) with points linestyle 3 title "RSSI val.", p(x) with lines linestyle 2 title "LS Regr.", h(x) with lines linestyle 1 title "AA"

print "Path loss constant n: ", n

pause -1 "Hit return to continue: (1)"