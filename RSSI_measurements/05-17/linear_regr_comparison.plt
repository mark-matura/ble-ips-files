# set terminal qt # Alternatives: qt (some OSX), wxt (gives top bar menu).
set terminal wxt
set output 'linear_regr_comparison.png'

set autoscale
set grid

# Set linestyles (line-points, line, points):
set style line 1 \
    linecolor rgb '#098c00' \
    linetype 1 linewidth 2 \
    pointtype 1 pointsize 0.5

set style line 2 \
    linecolor rgb '#405cc2' \
    linetype 1 linewidth 2

set style line 3 \
    linecolor rgb '#f0133c' \
    pointtype 2 pointsize 1

set xlabel "Distance to Server (m)"
set ylabel "RSSI (dBm)"

set title "Log Distance PLM with n of server 57:02"
n = 2

g(x) = -10 * n * log10(x) - 57.138798827827
b(x) = -10 * 1.4467751056939 * log10(x) - 57.138798827827

fit g(x) 'measurements.dat' using ($3):($2) via n

plot 'measurements.dat' using ($3):($2) with points linestyle 3 title 'RSSI Values', g(x) with lines linestyle 2 title 'NLLS', b(x) with lines linestyle 1 title 'LS'

print "Path loss constant n: ", n