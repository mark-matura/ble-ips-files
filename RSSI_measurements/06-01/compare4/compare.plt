# Gnuplot command file.
set terminal wxt # Alternatives: qt (some OSX), wxt (gives top bar menu).
set grid

set title "Measurement Correction Stream Overlayed On Raw Measurements with 5 measurement avg"
set xlabel "Time in ms since Jan 1. 1970"
set ylabel "RSSI (dBm)"
set autoscale
set key bottom right

# plot 'client' u 1:2 t 'client' w points lc 'red', 'gateway' u 1:2 t 'gateway' w points lc 'blue'
plot 'client' u 2:3 t 'client' w points lc 'red', 'gateway' u 2:3 t 'gateway' w points lc 'blue', 'corrected' u 2:3 t 'corrected' w points lc '#00c43e'