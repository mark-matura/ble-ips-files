# Gnuplot command file.
set terminal wxt # Alternatives: qt (some OSX), wxt (gives top bar menu).
set grid

set title "Measurement Stream of RT Corrected RSSI vs. Uncorrected RSSI"
set xlabel "Measurement Index"
set ylabel "RSSI (dBm)"
set autoscale
set key bottom right

plot 'client' u 1:2 t 'client' w linespoints lc 'red', 'gateway' u 1:2 t 'gateway' w linespoints lc 'blue', 'corrected' u 1:2 t 'corrected' w linespoints lc '#00c43e'