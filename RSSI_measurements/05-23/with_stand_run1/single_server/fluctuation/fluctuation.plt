# Gnuplot command file.
set terminal wxt # Alternatives: qt (some OSX), wxt (gives top bar menu).
set grid

set title "RSSI of Device 42:a2 With Stand"
set xlabel "Measurement Index"
set ylabel "RSSI (dBm)"
set autoscale
set key bottom right

plot 'fluctuation.csv' u 2 t 'Meas.' w linespoints lc rgb '#cf2721', -62.52 title 'Mean' lc 'black', -59.14 dashtype 2 lc 'black' title 'Std. Dev.', -65.9 dashtype 2 lc 'black' notitle