# https://datatofish.com/use-pandas-to-calculate-stats-from-an-imported-csv-file/

import pandas as pd
import glob

directory = "*.csv"
for fname in glob.glob(directory):
    print(fname)

    df = pd.read_csv(fname, header=None)

    mean = df[1].mean()
    max = df[1].max()
    min = df[1].min()
    count = df[1].count()
    median = df[1].median()
    std = df[1].std()
    var = df[1].var()

    print('Mean: ' + str(mean))
    print('Max: ' + str(max))
    print('Min: ' + str(min))
    print('Count: ' + str(count))
    print('Median: ' + str(median))
    print('Stddev: ' + str(std))
    print('Variance: ' + str(var))

    print(' ')