# http://psy.swansea.ac.uk/staff/carter/gnuplot/gnuplot_frequency.htm

set terminal wxt
set autoscale
set grid
set nokey
set boxwidth 0.5 absolute
set style fill solid 1.0 noborder
bin_width = 0.1;

bin_number(x) = floor(x/bin_width)
rounded(x) = bin_width * ( bin_number(x) + 0.5 )

set xlabel 'RSSI (dBm)'
set ylabel 'Frequency of RSSI'

plot 'distances/275' using (rounded($2)):(2) smooth frequency with boxes title 'Freq. of RSSI (dBm)'  linecolor 'blue'
