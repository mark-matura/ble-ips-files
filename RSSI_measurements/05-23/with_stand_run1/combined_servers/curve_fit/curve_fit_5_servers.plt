# Gnuplot command file.

set terminal wxt # Alternatives: qt (some OSX), wxt (gives top bar menu).
set grid

# Fit curve of path loss model to measurements via the path loss constant for my living room

set title "Comparison of LSR AA Hybrid Curve and Normal LSR Curve"
set xlabel "Distance (m)"
set ylabel "RSSI (dBm)"

m(x) = -10 * 1.7455770414078 * log10(x) - 59.97715407697
a(x) = -10 * 1.7455770414078 * log10(x) - 56.343361300691

plot 'combined_5_serv_normal.csv' u 3:2 t 'Measurements' w p lc '#cf2721', m(x) t 'LSR with anti-aliased y-intercept' w l lc '#1448d9', a(x) t 'LSR with fitted y-intercept' w l lc '#00a300'