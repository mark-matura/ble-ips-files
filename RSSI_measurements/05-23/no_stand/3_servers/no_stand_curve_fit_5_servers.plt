# Gnuplot command file.

set terminal wxt # Alternatives: qt (some OSX), wxt (gives top bar menu).
set grid

# Fit curve of path loss model to measurements via the path loss constant for my living room

set title "PLM Curve Fit of Device Data Without Stand Mount"
set xlabel "Distance (m)"
set ylabel "RSSI (dBm)"
set autoscale

m(x) = -10 * 1.896327859167 * log10(x) - 64.612477041055

plot '3_servers.csv' u 3:2 t 'Measurements' w p lc '#cf2721', m(x) t 'PLM coef. from LSR AA hybrid method' w l lc '#1448d9'