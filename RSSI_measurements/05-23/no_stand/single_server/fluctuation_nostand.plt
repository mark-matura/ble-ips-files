# Gnuplot command file.
set terminal wxt # Alternatives: qt (some OSX), wxt (gives top bar menu).
set grid
set key bottom right

set xlabel "Measurement Index"
set ylabel "RSSI (dBm)"
set autoscale
set offset 0,0,.5,.5

plot 'fluctuation_nostand.csv' u 2 t 'Without Stand' w linespoints lc 'red', -67.38 notitle lc 'red', -74.44 dashtype 2 lc 'red' notitle , -60.32  dashtype 2 lc 'red' notitle, 'fluctuation_with.csv' u 2 t 'With Stand' w linespoints lc 'blue', -62.52 notitle lc 'blue', -59.14 dashtype 2 lc 'blue' notitle, -65.9 dashtype 2 lc 'blue' notitle