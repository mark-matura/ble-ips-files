# https://datatofish.com/use-pandas-to-calculate-stats-from-an-imported-csv-file/

import pandas as pd
import glob

directory = "*.csv"
for fname in glob.glob(directory):
    print(fname)

    df = pd.read_csv(fname, header=None)

    mean = df[2].mean()
    max = df[2].max()
    min = df[2].min()
    count = df[2].count()
    median = df[2].median()
    std = df[2].std()
    var = df[2].var()

    print('Mean: ' + str(mean))
    print('Max: ' + str(max))
    print('Min: ' + str(min))
    print('Count: ' + str(count))
    print('Median: ' + str(median))
    print('Stddev: ' + str(std))
    print('Variance: ' + str(var))

    print(' ')