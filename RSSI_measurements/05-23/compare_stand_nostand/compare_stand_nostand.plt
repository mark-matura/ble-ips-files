# Gnuplot command file.
set terminal wxt
set grid

set xlabel "Measurement Index"
set ylabel "RSSI (dBm)"
set autoscale
set key bottom right

plot 'fluctuation_withstand.csv' u 2 t 'With Stand' w linespoints lc 'blue', -62.52 notitle lc 'blue', 'fluctuation_nostand.csv' u 2 t 'Without Stand' w linespoints lc 'red', -67.38 notitle lc 'red',