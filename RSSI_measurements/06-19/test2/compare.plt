# Gnuplot command file.
set terminal wxt # Alternatives: qt (some OSX), wxt (gives top bar menu).
set grid

set title "Measurement Stream With 1000 Measurement Pool Size At 1m"
set xlabel "Time in ms since Jan 1. 1970"
set ylabel "RSSI (dBm)"
set autoscale
set key bottom right

# plot 'client' u 1:2 t 'client' w points lc 'red', 'gateway' u 1:2 t 'gateway' w points lc 'blue'
plot 'client.txt' u 2:3 t 'client' w linespoints lc 'red', 'gateway.txt' u 2:3 t 'gateway' w linespoints lc 'blue', 'corrected.txt' u 2:3 t 'corrected' w linespoints lc '#00c43e', 'raw.txt' u 2:3 t 'raw' w linespoints lc 'yellow'