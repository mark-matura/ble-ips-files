# Gnuplot command file.
set terminal wxt # Alternatives: qt (some OSX), wxt (gives top bar menu).
set grid

set title "Measurement Stream With 500 Measurement Pool Size At 6m"
set xlabel "Time in ms since Jan 1. 1970"
set ylabel "RSSI (dBm)"
set autoscale
set key bottom right

plot 'client' u 2:3 t 'client' w linespoints lc 'red', 'gateway' u 2:3 t 'gateway' w linespoints lc 'blue'
# plot 'client' u 2:3 t 'client' w linespoints lc 'red', 'gateway' u 2:3 t 'gateway' w linespoints lc 'blue', 'corrected' u 2:3 t 'corrected' w linespoints lc '#00c43e'
