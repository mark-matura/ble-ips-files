# Gnuplot command file.
set terminal wxt
set encoding utf8

set grid
set datafile separator ","

set xlabel "Distance (m)"
set ylabel "RSSI (dBm)"
set key right top box opaque
set xrange [0:5]

a(x) = -10 * 2.2878627210583 * log10(x) -38.652654401932
b(x) = -10 * 2.2697222461197 * log10(x) -43.713755597199
c(x) = -10 * 2.8763567840022 * log10(x) -43.608559422742
d(x) = -10 * 1.9299819733783 * log10(x) -47.162352886614

plot a(x) t 'Beacon 1' w lines lc 'red', b(x) t 'Beacon 2' w lines lc 'blue', c(x) t 'Beacon 3' w lines lc 'dark-green', d(x) t 'Beacon 4' w lines lc 'black',