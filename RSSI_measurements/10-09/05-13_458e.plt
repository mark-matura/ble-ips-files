# Gnuplot command file.

set terminal qt # Alternatives: qt (some OSX), wxt (gives top bar menu).
set nokey
set grid

# Set linestyles (line-points, line, points):
set style line 1 \
    linecolor rgb '#098c00' \
    linetype 1 linewidth 1 \
    pointtype 1 pointsize 0.5

set style line 2 \
    linecolor rgb '#405cc2' \
    linetype 1 linewidth 1

set style line 3 \
    linecolor rgb '#f0133c' \
    pointtype 1 pointsize 1.5

set xlabel "Distance to Server (m)"
set ylabel "RSSI (dBm)"
set autoscale
# set initial value for n, the path loss constant for my living room
n = 2

# power is a function of the distance s
p(x) = -10 * n * log10(x) - 48.435483870968
h(x) = -10 * 1.6274738811666 * log10(x) - 48.435483870968

# Fit curve of path loss model to measurements via the path loss constant for my living room
fit p(x) '05-13_458e.dat' using ($1):($2) via n

set samples 400     # More precise function plotting.

set title "Log Distance PLM with n of server 45:8e"

plot '05-13_458e.dat' using ($1):($2) with points linestyle 3, p(x) with lines linestyle 2, h(x) with lines linestyle 1

print "Path loss constant n: ", n

pause -1 "Hit return to continue: (1)"