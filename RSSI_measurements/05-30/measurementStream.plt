# Gnuplot command file.
set terminal wxt # Alternatives: qt (some OSX), wxt (gives top bar menu).
set grid

set title "Measurement Stream of RT Corrected RSSI vs. Uncorrected RSSI"
set xlabel "Measurement Index"
set ylabel "RSSI (dBm)"
set autoscale
set key bottom right

plot 'dat/100' u 1 t '100 Msmnt Avg. ' w linespoints lc 'red', 'dat/20' t '20 Msmnt Avg.' w linespoints lc 'blue', 'dat/uncorr' t 'Uncorrected' w linespoints lc '#00c43e'