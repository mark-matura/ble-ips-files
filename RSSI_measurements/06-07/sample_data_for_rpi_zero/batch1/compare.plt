# Gnuplot command file.
set terminal wxt # Alternatives: qt (some OSX), wxt (gives top bar menu).
set grid

set title "Raw vs Averaged Client Measurements at 1m"
set xlabel "Time in ms since Jan 1. 1970"
set ylabel "RSSI (dBm)"
set autoscale
set key bottom right

# plot 'client' u 1:2 t 'client' w points lc 'red', 'gateway' u 1:2 t 'gateway' w points lc 'blue'
plot 'client' u 2:3 t 'averaged client' w linespoints lc 'red', 'raw' u 2:3 t 'raw client' w linespoints lc 'blue'