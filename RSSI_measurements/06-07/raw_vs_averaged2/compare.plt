set terminal wxt
set grid
set encoding utf8

set xlabel "Measurement Index"
set ylabel "RSSI (dBm)"
set autoscale
set key bottom right box opaque

plot 'client' u 3 t 'Moving Average' w linespoints lc 'red', 'raw' u 3 t 'Raw' w linespoints lc 'blue'