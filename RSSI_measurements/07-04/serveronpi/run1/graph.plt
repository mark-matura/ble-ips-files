# Gnuplot command file.
set terminal wxt
set encoding utf8

set grid
set datafile separator ","

set xlabel "Time Elapsed (s)"
set ylabel "RSSI (dBm)"
set autoscale
set key center right

plot 'mvavgclient.csv' u (($2 - 1593856379236) / 1000):($3) t 'Averaged Client' w linespoints lc 'red', 'mvavggateway.csv' u (($2 - 1593856379236) / 1000):($3) t 'Averaged Gateway' w linespoints lc 'blue', 'rtcorrected.csv' u (($2 - 1593856379236) / 1000):($3) t 'RT Corrected Client' w linespoints lc '#00c43e'