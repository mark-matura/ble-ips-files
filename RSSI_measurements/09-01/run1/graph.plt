# Gnuplot command file.
set title 'Measurement Pool with three Clients'
set terminal svg size 1500,600 fname 'Arial' fontscale 1.5
set output 'output.svg'
set encoding utf8

set grid
set datafile separator ","

set xlabel "Time in ms since Jan 1. 1970"
set ylabel "RSSI (dBm)"
set autoscale
set key bottom right box opaque

plot 'mvavgclient.csv' u 2:3 t 'AA client' w linespoints lc 'red'