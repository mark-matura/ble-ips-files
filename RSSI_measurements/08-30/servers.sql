-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: Aug 30, 2020 at 05:45 PM
-- Server version: 10.4.13-MariaDB-1:10.4.13+maria~bionic-log
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ble_ips_api`
--

-- --------------------------------------------------------

--
-- Table structure for table `servers`
--

CREATE TABLE `servers` (
  `id` int(10) UNSIGNED NOT NULL,
  `mac_addr` varchar(17) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference_path_loss` double NOT NULL DEFAULT -47.899677614236,
  `signal_propagation_exp` double NOT NULL DEFAULT 2.2421283703413,
  `x_coord` double NOT NULL DEFAULT 0,
  `y_coord` double NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `servers`
--

INSERT INTO `servers` (`id`, `mac_addr`, `reference_path_loss`, `signal_propagation_exp`, `x_coord`, `y_coord`, `created_at`, `updated_at`) VALUES
(1, '24:6f:28:7a:57:02', -42.992269109053, 2.0676734826131, 0, 3.98, '2020-08-30 11:30:40', '2020-08-30 12:07:34'),
(2, '24:6f:28:7a:48:2e', -39.036637566669, 2.1663846355787, 0, 0, '2020-08-30 11:30:40', '2020-08-30 11:57:58'),
(3, '24:6f:28:7a:54:16', -42.107757587865, 2.2089422315674, 3.28, 1.99, '2020-08-30 11:30:40', '2020-08-30 11:44:59'),
(4, '24:6f:28:7a:47:ee', -41.676658216241, 2.2678087018855, 0, 1.99, '2020-08-30 11:30:40', '2020-08-30 12:02:35'),
(5, '24:6f:28:7a:55:66', -42.482071981159, 2.1877304995866, 1.64, 0, '2020-08-30 11:30:40', '2020-08-30 11:53:44'),
(6, '24:6f:28:7a:38:ae', -46.345841281719, 2.4984152813137, 1.64, 3.98, '2020-08-30 11:30:40', '2020-08-30 11:34:13'),
(7, '24:6f:28:7a:45:8e', -34.444837281035, 1.9326394931154, 3.28, 3.98, '2020-08-30 11:30:40', '2020-08-30 11:40:09'),
(8, '24:6f:28:7a:41:3a', -40.887429242652, 2.6059219558825, 3.28, 0, '2020-08-30 11:30:41', '2020-08-30 11:49:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `servers`
--
ALTER TABLE `servers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `servers`
--
ALTER TABLE `servers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
