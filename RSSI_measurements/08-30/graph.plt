# Gnuplot command file.
set terminal wxt
set encoding utf8

set autoscale
set samples 400     # More precise function plotting.
set grid
set datafile separator ","

set xlabel "Distance (m)"
set xrange [0:5]
set ylabel "RSSI (dBm)"
set key top right box

a(x) = -10 * 2.0676734826131 * log10(x) + -42.992269109053
b(x) = -10 * 2.1663846355787 * log10(x) + -39.036637566669
c(x) = -10 * 2.2089422315674 * log10(x) + -42.107757587865
d(x) = -10 * 2.2678087018855 * log10(x) + -41.676658216241
e(x) = -10 * 2.1877304995866 * log10(x) + -42.482071981159
f(x) = -10 * 2.4984152813137 * log10(x) + -46.345841281719
g(x) = -10 * 1.9326394931154 * log10(x) + -34.444837281035
h(x) = -10 * 2.6059219558825 * log10(x) + -40.887429242652

plot a(x) t '24:6f:28:7a:57:02', b(x) t '24:6f:28:7a:48:2e', c(x) t '24:6f:28:7a:54:16', d(x) t '24:6f:28:7a:47:ee', e(x) t '24:6f:28:7a:55:66', f(x) t '24:6f:28:7a:38:ae', g(x) t '24:6f:28:7a:45:8e', h(x) t '24:6f:28:7a:41:3a'