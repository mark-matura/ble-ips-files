# Gnuplot command file.
set terminal wxt # Alternatives: qt (some OSX), wxt (gives top bar menu).
set grid

set title "Real time corrected RSSI based on antialiased gateway and client values"
set xlabel "Time in ms since Jan 1. 1970"
set ylabel "RSSI (dBm)"
set autoscale
set key bottom right

plot 'mvavgclient.txt' u 2:3 t 'AA client' w linespoints lc 'red', 'mvavggateway.txt' u 2:3 t 'AA gateway' w linespoints lc 'blue', 'rtcorrected.txt' u 2:3 t 'RT corrected client' w linespoints lc '#00c43e', 'rawclient.txt' u 2:3 t 'raw client' w points lc 'dark-red', 'rawgateway.txt' u 2:3 t 'raw gateway' w points lc 'dark-blue'