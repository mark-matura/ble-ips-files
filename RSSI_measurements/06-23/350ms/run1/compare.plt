# Gnuplot command file.
set terminal wxt # Alternatives: qt (some OSX), wxt (gives top bar menu).
set grid

set title "RT correction with 350ms advertising intervals"
set datafile separator ","

set xlabel "Time in ms since Jan 1. 1970"
set ylabel "RSSI (dBm)"
set autoscale
set key bottom right box opaque

plot 'mvavggateway.csv' u 2:3 t 'AA gateway' w linespoints lc 'blue', 'raw gateway' w points lc 'dark-blue'