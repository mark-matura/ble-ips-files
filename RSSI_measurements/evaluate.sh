# http://www.yourownlinux.com/2015/04/sed-command-in-linux-append-and-insert-lines-to-file.html

echo 'Sorting all values in measurement data files by time'

# set variables of file names and graph titles
FILENAME="server.log"
DIRECTORY="09-04"
TITLE="Averaged measurement pool with three clients over three seconds runtime"

# copy command files to directory
cp stats.py $DIRECTORY
cp graph.plt $DIRECTORY

cd $DIRECTORY

sed -i "1 a set title '$TITLE'" graph.plt

#find lines with tab separated data, sort by timestamp and delimit with commas
grep -o "rawclient.*$" $FILENAME | tee rawclient.csv
sort -t$'\t' -k2 -nr rawclient.csv | tee rawclient.csv
# sed  -i s/\ \ */,/g rawclient.csv

# grep -o "rawgateway.*$" $FILENAME | tee rawgateway.csv
# sort -t$'\t' -k2 -nr rawgateway.csv | tee rawgateway.csv
# sed  -i s/\ \ */,/g rawgateway.csv

grep -o "mvavgclient.*$" $FILENAME | tee mvavgclient.csv
sort -t$'\t' -k2 -nr mvavgclient.csv | tee mvavgclient.csv
# sed -i 's/\t/,/g' mvavgclient.csv

# grep -o "mvavggateway.*$" $FILENAME | tee mvavggateway.csv
# sort -t$'\t' -k2 -nr mvavggateway.csv | tee mvavggateway.csv
# sed -i 's/\t/,/g' mvavggateway.csv
<<<<<<< HEAD
=======
# sed  -i s/\ \ */,/g mvavggateway.csv
>>>>>>> 8bd8429efc2c622423ccb3b1477f6542a0b26038

# grep -o "rtcorrected.*$" $FILENAME | tee rtcorrected.csv
# sort -t$'\t' -k2 -nr rtcorrected.csv | tee rtcorrected.csv
# sed -i 's/\t/,/g' rtcorrected.csv
<<<<<<< HEAD
=======
# sed  -i s/\ \ */,/g rtcorrected.csv
>>>>>>> 8bd8429efc2c622423ccb3b1477f6542a0b26038

echo 'doing a statistical analysis'

# https://stackabuse.com/writing-to-a-file-with-pythons-print-function/
# python stats.py > statsdata.txt
# gnuplot "graph.plt"

# clean up directory
rm -rf stats.py