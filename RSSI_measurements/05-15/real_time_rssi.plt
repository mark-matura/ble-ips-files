set terminal qt # Alternatives: qt (some OSX), wxt (gives top bar menu).
set grid
set nokey
set format x
set format y

# Set linestyles (line-points, line, points):
set style line 1 \
    linecolor rgb '#098c00' \
    linetype 1 linewidth 1 \
    pointtype 1 pointsize 0.5


set style line 2 \
    linecolor rgb '#405cc2' \
    linetype 1 linewidth 1

set style line 3 \
    linecolor rgb '#f0133c' \
    pointtype 2 pointsize 1.5 \
    lt 1 lw 2 pi -1

set xlabel "Sampling times" font "Times-Roman, 16"
set xtics font ", 14"

set ylabel "RSSI (dBm)" font ", 16"
set ytics font ", 14"

set title "RSSI measurements from Peripheral 24:6f:28:7a:42:a2" font ", 18"
plot 'real_time_rssi_42a2.dat' using ($1) with linespoints  linestyle 3 title "RSSI val.",
pause -1 "Hit return to continue: (1)"

set title "RSSI measurements from Peripheral 24:6f:28:7a:57:02" font ", 18"
plot 'real_time_rssi_5702.dat' using ($1) with linespoints  linestyle 3 title "RSSI val.",

pause -1 "Hit return to continue: (2)"