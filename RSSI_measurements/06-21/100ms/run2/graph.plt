# Gnuplot command file.
set title 'RT corrections with 100ms BLE Advertising Interval'
set terminal svg size 1500,600 fname 'Arial' fontscale 1.5
set output 'output.svg'

set grid
set datafile separator ","

set xlabel "Time in ms since Jan 1. 1970"
set ylabel "RSSI (dBm)"
set autoscale
set key bottom right box opaque

plot 'mvavgclient.csv' u 2:3 t 'AA client' w linespoints lc 'red', 'mvavggateway.csv' u 2:3 t 'AA gateway' w linespoints lc 'blue', 'rtcorrected.csv' u 2:3 t 'RT corrected client' w linespoints lc '#00c43e', 'rawclient.csv' u 2:3 t 'raw client' w points pointtype 1 lc 'dark-red', 'rawgateway.csv' u 2:3 t 'raw gateway' w points pointtype 1 lc 'dark-blue'