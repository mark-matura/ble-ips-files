import json
import math
import matplotlib.pyplot as plt
import csv


def calculateAverages(measurements):
    measurements = sorted(measurements, key=lambda measurement: measurement[0])
    currentDistance = 0.5
    distanceBuffer = []
    xy = []
    printBuffer = ""

    for measurement in measurements:
        if measurement[0] != currentDistance:
            printBuffer += "%f   %f\n" % (currentDistance, average(distanceBuffer))
            xy.append([currentDistance, average(distanceBuffer)])
            distanceBuffer = []
            currentDistance = measurement[0]
        distanceBuffer.append(measurement[1])

    printBuffer += "%f   %f\n" % (
        measurement[0],
        average(distanceBuffer),
    )

    xy.append([currentDistance, average(distanceBuffer)])
    writeToFile(printBuffer)

    return xy


def average(list):
    return sum(map(float, list)) / len(list)


def sessionToList(session) -> list:
    measurements = [json.loads(line) for line in session]

    measurements = [
        [
            calibrationMeasurement["distance"],
            calibrationMeasurement["rssi"],
            calibrationMeasurement["server_addr"],
        ]
        for calibrationMeasurement in measurements
    ]
    return measurements


def writeToFile(buffer: str):
    f = open("calibration-data/src/" + measurements[0][2] + ".dat", "w")
    f.write(buffer)
    f.close()


def getServerInfo():
    with open("calibration-data/src/servers.csv", "r") as file:
        reader = csv.DictReader(file, delimiter="\t")
        return [dict(row) for row in reader]


def float_range(start, stop, step):
    while start < stop:
        yield float(start)
        start += step


if __name__ == "__main__":
    serverInfo = getServerInfo()

    ax = plt.axes()
    ax.set_xscale("log")

    ax.set_xlim(0.4, 4)
    ax.set_ylim(-65, -30)

    ax.set_xlabel("Distance (m)")
    ax.set_ylabel("RSSI (dBm)")

    ctr = 1
    for server in serverInfo:
        rssid0 = float(server["reference_path_loss"])
        n = float(server["signal_propagation_exp"])
        name = str(server["mac_addr"])

        with open("calibration-data/src/calibration:" + name + ".json") as f:
            session = json.load(f)
            measurements = sessionToList(session)
            xy = calculateAverages(measurements)

        x = [x for x in list(float_range(0.1, 5, 0.5))]
        y = [rssid0 - 10 * n * math.log(x, 10) for x in x]

        distances = [xy[0] for xy in xy]
        rssiVals = [xy[1] for xy in xy]

        # plot lines
        ax.plot(x, y, label="Beacon " + str(ctr))

        # plot points
        ax.scatter(distances, rssiVals, marker="x")
        ctr += 1

    ax.legend()
    plt.show()