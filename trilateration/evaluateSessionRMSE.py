import json
import math
import os

session = None


def sessionToList(session) -> list:
    x = session["actual"]["x"]
    y = session["actual"]["y"]
    print("Actual:      %f / %f" % (x, y))

    coordinatesList = [
        [position["x"], position["y"]] for position in session["positions"]
    ]
    return coordinatesList


def average(list):
    return sum(map(float, list)) / len(list)


def rmse(coordinatesList) -> float:
    x = session["actual"]["x"]
    y = session["actual"]["y"]

    sum = 0
    for coordinates in coordinatesList:
        deltax = abs(x - coordinates[0])
        deltay = abs(y - coordinates[1])

        # euclidean distance between actual and measured (estimated) position
        sum += deltax ** 2 + deltay ** 2

    return math.sqrt(sum / len(coordinatesList))


def getRaw(coordinatesList):

    xvals = [coordinates[0] for coordinates in coordinatesList]
    yvals = [coordinates[1] for coordinates in coordinatesList]

    avgx = average(xvals)
    avgy = average(yvals)
    rmseVal = rmse(coordinatesList)

    print("RAW AVG:     %f / %f" % (avgx, avgy))
    print("RAW RMSE:    %f" % (rmseVal))

    return rmseVal


def getInterpolated(coordinatesList):
    xvals = [coordinates[0] for coordinates in coordinatesList]
    yvals = [coordinates[1] for coordinates in coordinatesList]

    xvalsRollingAvg = []
    yvalsRollingAvg = []

    valsRollingAvg = []

    counter = 0
    while counter < 494:
        counter += 1

        if counter <= 4:  # ignore first 5, because not full window
            continue

        # slice list to get window of size 10
        xvalues = xvals[counter - 5 : counter + 5]
        yvalues = yvals[counter - 5 : counter + 5]

        # new list contains almost 500 values that are rolling average -> "interpolation"
        # same as done in paper with the time series data, this was done once a second with 10 values
        xvalsRollingAvg.append(average(xvalues))
        yvalsRollingAvg.append(average(yvalues))

        # saves as coordinate tuples
        valsRollingAvg.append([average(xvalues), average(yvalues)])

    # calculate global mean XY, should not differ greatly from raw XY mean
    avgx = average(xvalsRollingAvg)
    avgy = average(yvalsRollingAvg)

    rmseVal = rmse(valsRollingAvg)

    print("INT AVG:     %f / %f" % (avgx, avgy))
    print("INT RMSE:    %f" % (rmseVal))

    return rmseVal


if __name__ == "__main__":
    rawRmse = []
    interpolatedRmse = []

    for filename in os.listdir("trilateration/src"):
        if filename.endswith(".json"):
            with open("trilateration/src/" + filename) as f:
                session = json.load(f)
                coordinatesList = sessionToList(session)

                rawRmse.append(getRaw(coordinatesList))
                interpolatedRmse.append(getInterpolated(coordinatesList))

                print()

    print("GLOBAL RAW RMSE:    %f" % (average(rawRmse)))
    print("GLOBAL INT RMSE:    %f" % (average(interpolatedRmse)))