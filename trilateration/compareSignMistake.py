import json
import math
import os
import numpy as np
from scipy.optimize import fmin_powell
import matplotlib.pyplot as plt

session = None


def sessionToList(session) -> list:
    return [position for position in session["positions"]]


def search(mac_addr, servers):
    for server in servers:
        if mac_addr == server["mac_addr"]:
            return server


def findPositionFromDistances(positions):
    x_coord = session["actual"]["x"]
    y_coord = session["actual"]["y"]

    coordinates = []
    erroneousCoordinates = []

    for position in positions:
        servers = [
            [server["mac_addr"], server["distance"]] for server in position["server"]
        ]

        # contains info about server XY, not just distances to them from position
        detailed_servers = [server for server in session["servers"]]

        for server in servers:
            detailed_server = search(server[0], detailed_servers)
            server.append(detailed_server["x"])
            server.append(detailed_server["y"])

        closestServers = getClosestThreeServers(servers)

        # reduce mean squared error passing closest three servers as an argument
        positionCoordinates = fmin_powell(
            meanSquaredError, (1.5, 2), args=(closestServers,), disp=False
        )

        coordinates.append([positionCoordinates[0], positionCoordinates[1]])

        # reduce mean squared error passing closest three servers as an argument
        positionCoordinates = fmin_powell(
            wrongMSE, (1.5, 2), args=(closestServers,), disp=False
        )

        erroneousCoordinates.append([positionCoordinates[0], positionCoordinates[1]])
    return coordinates, erroneousCoordinates


# Code taken from trilaterate.py and modified to query from file
def getClosestThreeServers(servers):
    if len(servers) < 3:
        raise ValueError("Less than three servers supplied to getClosestThreeServers")

    sortedServers = sorted(
        servers, key=lambda server: getDistance(server), reverse=False
    )

    return sortedServers[0:3]


def getDistance(server) -> float:
    return server[1]


def meanSquaredError(x, bestServers):
    """Return MSE between measured and computed distance.
    The goal is to optimize this in respect to X as a 1D array

    https://nrr.mit.edu/sites/default/files/documents/Lab_11_Localization.html#sec1b
    """
    meanSquaredError: float = 0.0

    for server in bestServers:
        squaredRadius: float = getDistance(server) ** 2

        x_coord = server[2]
        y_coord = server[3]

        xError = (x[0] - x_coord) ** 2
        yError = (x[1] - y_coord) ** 2

        meanSquaredError += (squaredRadius - xError - yError) ** 2
    return meanSquaredError


def wrongMSE(x, bestServers):
    """Return MSE between measured and computed distance.
    The goal is to optimize this in respect to X as a 1D array

    https://nrr.mit.edu/sites/default/files/documents/Lab_11_Localization.html#sec1b
    """
    meanSquaredError: float = 0.0

    for server in bestServers:
        squaredRadius: float = getDistance(server) ** 2

        x_coord = server[2]
        y_coord = server[3]

        xError = (x[0] - x_coord) ** 2
        yError = (x[1] - y_coord) ** 2

        meanSquaredError += (squaredRadius - xError + yError) ** 2
    return meanSquaredError


def average(list):
    return sum(map(float, list)) / len(list)


if __name__ == "__main__":
    rawRmse = []
    interpolatedRmse = []
    filename = "3.4_3.7.json"

    ax = plt.axes()

    plt.grid(True, linestyle="--")
    plt.gca().set_aspect("equal", adjustable="box")
    plt.rc("font", family="serif")

    ax.set_xlim(0, 4.17)
    ax.set_ylim(0, 5.4)
    # for filename in os.listdir("trilateration/src"):
    # if filename.endswith(".json"):

    with open("trilateration/src/" + filename) as f:
        session = json.load(f)
        serverDistances = sessionToList(session)

        x = session["actual"]["x"]
        y = session["actual"]["y"]

        ax.scatter(x, y, marker="s", label="Actual Position")

        correct, erroneous = findPositionFromDistances(serverDistances)

        x = [coordinates[0] for coordinates in correct]
        y = [coordinates[1] for coordinates in correct]

        ax.scatter(average(x), average(y), marker=".", label="Mean Correct")
        ax.scatter(x, y, marker=".", label="Correct MSE")
        print(np.std(x), np.std(y))
        print(average(x), average(y))

        x = [coordinates[0] for coordinates in erroneous]
        y = [coordinates[1] for coordinates in erroneous]

        ax.scatter(average(x), average(y), marker=".", label="Mean Erroneous")
        ax.scatter(x, y, marker=".", label="Erroneous MSE")
        print(average(x), average(y))
        print(np.std(x), np.std(y))

    ax.legend()
    plt.show()