%% Trilateration mit Messfehler (2-dimensional)
% bezieht sich auf die Maturaarbeit von Mark Marolf
% Kanti Baden vom 30.10.2020
% "A Buletooth Loe Energy-Based Indoor Positiong System"
% eingereicht bei Schweizer Jugend forscht

% Die Methode der kleinsten Fehlerquadrate wurde vereinfacht
% mit 3 Beacons B1, B2 und B3 mit Koordinaten (Bxi,Byi)
% P   : Position des Observers mit Koordinaten (Px,Py)
% Di  : wahrer Abstand von P nach Bi (i = 1,2,3)
% dDi : Messunsicherheit bei der Messung von P nach Bi
% L = Summe ((Di + dDi)^2 - (Rx - Bx)^2 - (Ry - By)^2)
%     �ber alle Beacons
% Von dieser Summe wird R = (Rx,Ry) gesucht, so das L minimal wird
% dies Methode wird mit der Funktion fs.m beschrieben
% Dieses Verfahren wird f�r eine Menge von Fehlern wiederholt
% und als "Punktwolke" dargestellt. Die Messungen werden gemittelt
% und auch wieder dargestellt.

% Die Methode wird verglichen mit dem Fehler der Mark Marolf unterlief.
% In der Summe L hatte er ein falsches Vorzeichen bei der y-Komponente.
% L2 = Summe ((Di + dDi)^2 - (R2x - Bx)^2 + (R2y - By)^2)
%     �ber alle Beacons und alle Messungen
% Von dieser Summe wird R2 = (R2x,R2y) gesucht, so das L2 minimal wird
% dies Methode wird mit der Funktion fs2.m beschrieben

% alle Variabeln l�schen
clear all
clc

% Globale Variablen
global B D
global j1 j2 j3

% Beacons
B = [  0     0,
     400     0,
     200   300];
xmin = min(B(:,1)) - 20;
xmax = max(B(:,1)) + 20;
ymin = min(B(:,2)) - 20;
ymax = max(B(:,2)) + 20;

% Observer
P = [180 100];

% Messfehler
eps = 20;    % Messunsicherheit zu Beacon 1, 2 und 3
dD = [-eps 0 eps];

for i = 1:3       % alle Beacons
  for j = 1:3     % alle Fehler
    D(i, j) = norm(P - B(i,:)) + dD(j);
  end;
end;

% suchen des Minimums
R = zeros(27,2);
R2 = zeros(27,2);

x0 = P;    % Observerposition und Startwert
txt1 = ["Observer = (" num2str(x0(1)) ", " num2str(x0(2)) ") cm"];
disp(txt1)
disp("")

for j1 = 1:3
  for j2 = 1:3
    for j3 = 1:3
      i = (j1-1)*9 + (j2-1)*3 + j3;
      R(i,:) = fminsearch (@fs, x0);
      R2(i,:) = fminsearch (@fs2, x0);
    end;
  end;
end;

% Auswertung der Abweichungen mit korrekter Formel
disp(["Messfehler = " num2str(eps) " cm"])
disp("")
disp("Auswertung der Abweichungen mit korrekter Formel")
R_mean = [mean(R(:,1)) mean(R(:,2))];
err_R = norm(R_mean - P);
stdv_R = mean(std(R(:,:)));
disp(["Messung R = (" num2str(R_mean(1)) ", " num2str(R_mean(2)) ...
      ") cm / Abweichung = " num2str(err_R) ...
      " cm  / Stdv = " num2str(stdv_R) " cm"])
disp("")

% Auswertung der Abweichungen mit falscher Formel
disp("Auswertung der Abweichungen mit falscher Formel")
R2_mean = [mean(R2(:,1)) mean(R2(:,2))];
err_R2 = norm([mean(R2(:,1)) mean(R2(:,2))] - P);
stdv_R2 = mean(std(R2(:,:)));
disp(["Messung R2 = (" num2str(R2_mean(1)) ", " num2str(R2_mean(2)) ...
      ") cm / Abweichung = " num2str(err_R2) ...
      " cm  / Stdv = " num2str(stdv_R2) " cm"])

% grafische Darstellung der Resultate
clf       % Grafik l�schen
% Beacons Dreiecke Cyan
plot(B(:,1),B(:,2),"^c","markersize",10)
hold on
% Observer wahre Position Kreis Blau
plot(P(:,1),P(:,2),"ob")
% Bestimmung der Position mit richtiger Formel Punktewolke Gr�n
plot(R(:,1),R(:,2),".g")
% Mittelwert der Messung mit richtiger Formel Kreuz gr�n
plot(R_mean(1),R_mean(2),"+g")
% Bestimmung mit falscher Formel Punkt Rot
plot(R2(:,1),R2(:,2),".r")
% Mittelwert der Messung mit falscher Formel Kreuz rot
plot(R2_mean(1),R2_mean(2),"+r")
axis equal
axis([xmin xmax ymin ymax])
grid minor on
hold off
txt = ["Trilateration mit richtiger Formel (gr�n) und falscher Formel (rot)" ...
       " / Fehler = " num2str(eps) " cm"];
title(txt)
print tri_error.jpg
disp("")
