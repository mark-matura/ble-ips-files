function L = fs2 (x)
  % Summe der Fehlerquadrate
  % mit falscher Formel von Kantischüler
  % Steinhausen, 14.12.2020, Zeno Stössel
  global D B j1 j2 j3
  % disp(x);
  L = (D(1,j1)^2 - (x(1) - B(1,1))^2 + (x(2) - B(1,2))^2)^2 + ...
      (D(2,j2)^2 - (x(1) - B(2,1))^2 + (x(2) - B(2,2))^2)^2 + ...
      (D(3,j3)^2 - (x(1) - B(3,1))^2 + (x(2) - B(3,2))^2)^2;
endfunction