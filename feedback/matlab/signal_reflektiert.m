% Signal mit Einfachreflexion
% Sender und Empf�nger auf H�he h
% Sender und Empf�nger in Distanz d
% Reflexionsfaktor r (r > 0, kein Phasensprung und r < 0, mit Phasensprung)
%
% Steinhausen, 18.01.2021, Zeno St�ssel

r = 0.5;            % Reflexionsfaktor
h = 1;              % H�he in m
d = 0.5:0.001:10;   % Abstand in m
c = 3E8;            % Lichtgeschwindigkeit
f = 2.4E9;          % Frequenz BLE
lambda = c/f;       % Wellenl�nge
k = 2*pi/lambda;    % Wellenzahl
P0 = 10E-3;         % Sendeleistung im Abstand von d0
d0 = 1;             % Referenzabstand

% Distanz des Reflektierten Signales
d2 = 2*sqrt((d/2).^2 + h^2);

% Signal 1 mit Phase ohne Reflektion
E1 = P0*((d0./d).^2).* exp(-1i*k*d); 

% Signal 2 mit Phase, d.h. reflektiertes Signal
E2 = P0*r*((d0./d2).^2).* exp(-1i*k*d2); 

% Leistung des Signals ohne Reflektion
P1 = abs(E1);
rssi1 = 10*log(P1);

% Leistung des Signals mit Reflektion
P = abs(E1+E2);
rssi = 10*log(P);

% Darstellung
figure();
clf;
semilogx(d,rssi,"r",d,rssi1,":r")
grid on
txt = ["Signal mit Einfachreflexion, r = " num2str(r)];
title(txt)
xlabel("Distance d [m]")
ylabel("RSSI")
print signal_reflektiert.jpg