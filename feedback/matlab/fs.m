function L = fs (x)
  % Summe der Fehler Quadrate
  % Steinhausen, 14.12.2020, Zeno St�ssel
  global D B j1 j2 j3
  L = (D(1,j1)^2 - norm(x - B(1,:))^2)^2 + ...
      (D(2,j2)^2 - norm(x - B(2,:))^2)^2 + ...
      (D(3,j3)^2 - norm(x - B(3,:))^2)^2;
endfunction