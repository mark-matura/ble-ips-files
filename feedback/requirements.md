---
fontfamily: mathpazo
author-meta: "Mark Marolf"
geometry: [a4paper, bindingoffset=0mm, inner=30mm, outer=30mm, top=30mm, bottom=30mm] # See https://ctan.org/pkg/geometry for more options
lang: de-CH

colorlinks: true # See https://ctan.org/pkg/xcolor for colors

header-includes: |
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyhead[LO,LE]{21. Februar 2021}
    \fancyhead[RO,RE]{Mark Marolf}
    \linespread{1.1}
---

# Anpassungen an SJF Bericht

Link zum angepassten Paper:
[https://gitlab.com/mark-matura/ble-ips-files/-/blob/master/paper/ipsreport.pdf](https://gitlab.com/mark-matura/ble-ips-files/-/blob/master/paper/ipsreport.pdf)

1. Messungen zum "Log-Distance Path Loss Modell in Darstellung mit Distanz logarithmisch und RSSI linear eintragen, d.h. Figur 2.9 ersetzten bzw. ergänzen
    - Abb. 2.9 mit log. skalierter X-Achse ersetzt. Messdaten und Auswertungsskript sind auf [Gitlab](https://gitlab.com/mark-matura/ble-ips-files/-/tree/master/calibration-data) zu finden.


2. Fehler im Text korrigieren (werden dem Kandidaten noch übermittelt)
    - Titelblatt neu
    - Formel 2.8, 2.9 angepasst, nun hat es Wurzeln für Pythagoras
    - Formel 2.10 mit Fehler $\rho_i$ eingefügt, bzw tatsächlich benützte in Formel 2.11 mit $\delta_i$ mit kurzem Begleittext
    - Formel 3.1 angepasst, nun skalare multiplikation (1 / n) mit Summe der X/Y Komponenten klar -> Schnitt.
    - Tabelle 4.1 und 4.2 angepasst mit zusätzlichen Messdaten


3. Position (Ist-Position), mit "Wolke" von mehreren Bestimmungen und Mittelwert dieser Bestimmungen mit falscher und mit richtiger Formel einfügen.
    - Abb. 4.6 und 4.7 mit kurzem Begleittext. Rohdaten und Auswertungsskript [hier](https://gitlab.com/mark-matura/ble-ips-files/-/tree/master/trilateration)


4. Fehlerbetrachtung mit RMSE korrigieren bzw. verwendete Formel einfügen"
    - Abb. 4.1 eingefügt, Form. 4.1 mit RMSE formel eingefügt