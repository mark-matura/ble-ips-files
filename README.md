# ble-ips-files
This repository stores files that are of secondary importance to the BLE IPS. All files for 3D printing are stored in 3D_Objects, sequence diagram source code is stored in sequence_diagrams and raw measurement telemetry are stored in RSSI_measurements.

## 3D Objects
Due to its simplicity and ease of use, all cases were designed using [Tinkercad](https://www.tinkercad.com/). The Gcode is optimized for the Monoprice Select mini v2 with Velleman Delta PLA filament. For best results I recommend using gluestick on the printing bed. The walls of the center square mount need to be adapted to the size and shape of your own mount. The CAD file is located at [https://www.tinkercad.com/things/e8bipnYDLBF-esp32-ips-beacon-stand-adapter](https://www.tinkercad.com/things/e8bipnYDLBF-esp32-ips-beacon-stand-adapter) and [https://www.tinkercad.com/things/ieZnQSQcqZM-rpi-zero-w-ips-client-case](https://www.tinkercad.com/things/ieZnQSQcqZM-rpi-zero-w-ips-client-case).

## Measurement files
The ble-ips-api outputs measurements to the server log files in /storage/logs/* for easy sorting ie.
```
[2020-06-02 11:02:55] local.INFO: corrected     1591092149068   -50.364
[2020-06-02 11:02:55] local.INFO: client        1591092149352   -55
[2020-06-02 11:02:55] local.INFO: gateway       1591092149396   -55
[2020-06-02 11:02:55] local.INFO: corrected     1591092149352   -54.364
[2020-06-02 11:02:55] local.INFO: client        1591092147920   -50
[2020-06-02 11:02:55] local.INFO: gateway       1591092147914   -54
[2020-06-02 11:02:55] local.INFO: corrected     1591092147920   -50.364
[2020-06-02 11:02:55] local.INFO: client        1591092148742   -53
[2020-06-02 11:02:55] local.INFO: gateway       1591092148732   -54
[2020-06-02 11:02:55] local.INFO: corrected     1591092148742   -53.364
[2020-06-02 11:02:55] local.INFO: client        1591092148415   -53
[2020-06-02 11:02:55] local.INFO: gateway       1591092148464   -55
[2020-06-02 11:02:55] local.INFO: corrected     1591092148415   -52.364
[2020-06-02 11:02:55] local.INFO: client        1591092149088   -53
```

GnuPlot is used to draw graphs using the measurements. I supply different templates in the specific folder that you can use at your disposal. As an example of how to move forward, here is how to evaluate data.

```sh
$ tail -f storage/logs/lumen-2020-06-07.log # write operations on logfile are output to stdout

# when finished just copy all data into a normal ascii text file ie
$ cat storage/logs/lumen-2020-06-07.log | tee myfiles/allmeasurements
# adapt the sort file with your file name (allmeasurements) and directory (myfiles) to point
# sorter to your data file with server logs using
$ nano sort.sh
# run sorting algorithm that creates new files with measurements of same origin sorted by timestamp.
$ ./sort.sh
#open gnuplot
$ gnuplot
gnuplot> load 'compare.plt' # or any other plot file you want, once again I have a few templates
```

Gnuplot has the best SVG exports when running WXT as a terminal type. If you are feeling adventurous, you can try stuff like pngcairo for better results. It crashes my mingw64 terminal though.

## Sequence diagrams
Graphical documentation is nice I guess. I made all my sequence diagrams with [sequencediagram.org ](https://sequencediagram.org/). Just copy the txt source code into the field on the left and you can edit and save own versions of my diagrams.